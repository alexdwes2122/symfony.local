<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211215110916 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_2741493C3A909126 ON contacto');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2741493C3A909126C1E70A7F ON contacto (nombre, telefono)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_2741493C3A909126C1E70A7F ON contacto');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2741493C3A909126 ON contacto (nombre)');
    }
}
