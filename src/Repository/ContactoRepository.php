<?php

namespace App\Repository;

use App\Entity\Contacto;
use App\Entity\Usuario;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contacto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacto[]    findAll()
 * @method Contacto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacto::class);
    }

    private function addUserFilter(QueryBuilder $qb, Usuario $usuario)
    {
        if (in_array('ROLE_ADMIN', $usuario->getRoles()) === false) {
            $qb->innerJoin('contacto.usuario', 'usuario')
                ->andWhere($qb->expr()->eq('contacto.usuario', ':usuario'))
                ->setParameter('usuario', $usuario);
        }
    }
    /**
    * @return Contacto[] Returns an array of Contacto objects
    */
    public function findContactos(
        ?Usuario $usuario, ?string $nombre,
        ?string $fechaInicial, ?string $fechaFinal,
        ?string $ciudad, ?string $telefono,
        ?string $order)
    {
        $qb = $this->createQueryBuilder('contacto');

        if (!is_null($ciudad) && $ciudad !== '') {
            $qb->innerJoin('contacto.ciudad', 'ciudad');

            $qb ->andWhere(
                    $qb->expr()->like('ciudad.nombre', ':ciudad')
            )->setParameter('ciudad', '%'.$ciudad.'%');
        }

        if (!is_null($nombre) && $nombre !== '') {
            $qb ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('contacto.nombre', ':val'),
                    $qb->expr()->like('contacto.descripcion', ':val')
                )
            )->setParameter('val', '%'.$nombre.'%');
        }

        if (!is_null($telefono) && $telefono !== '') {
            $qb ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('contacto.telefono', ':val')
                )
            )->setParameter('val', '%'.$telefono.'%');
        }

        if (!is_null($fechaInicial) && $fechaInicial !== '') {
            $dtFechaInicial = DateTime::createFromFormat('Y-m-d', $fechaInicial);

            $qb ->andWhere($qb->expr()->gte('contacto.fechaNacimiento', ':fechaInicial'))
                ->setParameter('fechaInicial', $dtFechaInicial);
        }

        if (!is_null($fechaFinal) && $fechaFinal !== '') {
            $dtFechaFinal = DateTime::createFromFormat('Y-m-d', $fechaFinal);

            $qb ->andWhere($qb->expr()->lte('contacto.fechaNacimiento', ':fechaFinal'))
                ->setParameter('fechaFinal', $dtFechaFinal);
        }

        if (!is_null($usuario))
            $this->addUserFilter($qb, $usuario);

        if (!is_null($order)) {
            $qb->addOrderBy('contacto.' . $order, 'ASC');
        }

        return $qb->getQuery()->getResult();
    }

    public function findContactosConCiudad(string $ordenacion, string $tipoOrdenacion, Usuario $usuario) {
        $qb = $this->createQueryBuilder('contacto');

        $qb->addSelect('ciudad')
            ->innerJoin('contacto.ciudad', 'ciudad')
            ->orderBy('contacto.' . $ordenacion, $tipoOrdenacion);

        $this->addUserFilter($qb, $usuario);

        return $qb->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Contacto
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
