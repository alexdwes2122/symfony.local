<?php

namespace App\BLL;

use App\Entity\Usuario;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UsuarioBLL extends BaseBLL
{
    public function nuevo(string $email, string $password)
    {
        $usuario = new Usuario();
        $usuario->setPassword($this->encoder->hashPassword($usuario, $password));
        $usuario->setEmail($email);
        $usuario->setRoles(['ROLE_USER']);

        return $this->guardaValidando($usuario);
    }

    public function profile()
    {
        $usuario = $this->getUser();

        return $this->toArray($usuario);
    }

    public function cambiaPassword(string $nuevoPassword) : array
    {
        $usuario = $this->getUser();
        $usuario->setPassword($this->encoder->hashPassword($usuario, $nuevoPassword));
        return $this->guardaValidando($usuario);
    }

    public function toArray(Usuario $usuario) : array
    {
        if ( is_null ($usuario))
            throw new \Exception("No existe el usuario");
        if (!($usuario instanceof Usuario))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $usuario->getId(),
            'email' => $usuario->getEmail(),
            'roles' => $usuario->getRoles()
        ];
    }
}