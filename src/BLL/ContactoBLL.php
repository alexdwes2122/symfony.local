<?php

namespace App\BLL;

use App\Entity\Ciudad;
use App\Entity\Contacto;
use App\Entity\Usuario;
use App\Repository\CiudadRepository;
use App\Repository\ContactoRepository;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;

class ContactoBLL extends BaseBLL
{
    public function getContactosConOrdenacion(?string $ordenacion)
    {
        if (!is_null($ordenacion)) {
            $tipoOrdenacion = 'asc';
            $session = $this->requestStack->getSession();
            $contactosOrdenacion = $session->get('contactosOrdenacion');
            if (!is_null($contactosOrdenacion)) {
                if ($contactosOrdenacion['ordenacion'] === $ordenacion)
                {
                    if ($contactosOrdenacion['tipoOrdenacion'] === 'asc')
                        $tipoOrdenacion = 'desc';
                }
            }

            $session->set('contactosOrdenacion', [
                'ordenacion' => $ordenacion,
                'tipoOrdenacion' => $tipoOrdenacion
            ]);
        } else {
            $ordenacion = 'id';
            $tipoOrdenacion = 'asc';
        }

        $usuarioLogueado = $this->security->getUser();
        return $this->em->getRepository(Contacto::class)->findContactosConCiudad(
            $ordenacion, $tipoOrdenacion, $usuarioLogueado);
    }

    public function actualizaContacto(Contacto $contacto, array $data)
    {
        $urlImagen = $this->getImagenContacto($data);
        $ciudad = $this->em->getRepository(Ciudad::class)->find($data['ciudad']);
        $usuario = $this->getUser();
        $contacto->setNombre($data['nombre']);
        $contacto->setTelefono($data['telefono']);
        $contacto->setImagen($urlImagen);
        $contacto->setEmail($data['email']);
        $contacto->setCiudad($ciudad);
        $fechaNacimiento = DateTime::createFromFormat('d/m/Y', $data['fechaNacimiento']);
        $contacto->setFechaNacimiento($fechaNacimiento);
        $contacto->setUsuario($usuario);

        return $this->guardaValidando($contacto);
    }

    private function getImagenContacto(array $data)
    {
        $arr_imagen = explode (',', $data['imagen']);
        if ( count ($arr_imagen) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imagen = base64_decode ($arr_imagen[1]);
        if (is_null($imagen))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $fileName = $data['nombre'].'.jpg';
        $filePath = $this->images_directory . $fileName;
        $urlImagen = $this->images_url . $fileName;
        $ifp = fopen ($filePath, "wb");
        if (!$ifp)
            throw new BadRequestHttpException('No se ha podido guardar la imagen');

        $ok = fwrite ($ifp, $imagen);
        if ($ok === false)
            throw new BadRequestHttpException('No se ha podido guardar la imagen');

        fclose ($ifp);

        return $urlImagen;
    }

    public function nuevo(array $data) {
        $contacto = new Contacto();
        return $this->actualizaContacto($contacto, $data);
    }

    public function getContactos(?string $order, ?string $nombre, ?string $ciudad, ?string $telefono)
    {
        $usuario = $this->getUser();
        $contactos = $this->em->getRepository(Contacto:: class )->findContactos(
            $usuario, $nombre, $fechaInicial=null, $fechaFinal=null, $ciudad, $telefono, $order);

        return $this->entitiesToArray($contactos);
    }

    public function toArray(Contacto $contacto)
    {
        if ( is_null ($contacto))
            return null;

        return [
            'id' => $contacto->getId(),
            'nombre' => $contacto->getNombre(),
            'telefono' => $contacto->getTelefono(),
            'email' => $contacto->getEmail(),
            'imagen' => $contacto->getImagen(),
            'ciudad' => $contacto->getCiudad()->getNombre(),
            'fechaNacimiento' => is_null($contacto->getFechaNacimiento()) ? '' : $contacto->getFechaNacimiento()->format('d/m/Y'),
            'usuario' => $contacto->getUsuario()->getEmail()
        ];
    }

    public function checkAccessToContacto(Contacto $contacto)
    {
        if ($this->checkRoleAdmin() === false) {
            $usuario = $this->getUser();
            if ($usuario->getId() !== $contacto->getUsuario()->getId())
                throw new AccessDeniedHttpException();
        }
    }
}