<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="dwes_index")
     */
    public function index()
    {
        $nombre = 'Álex';
        $alumnos = [
            'Ana',
            'Jonathan',
            'Pedro'
        ];
        return $this->render('hola-mundo.html.twig', [
            'nombre' => $nombre,
            'alumnos' => $alumnos,
            'numero' => 5,
            'fecha' => new \DateTime()
        ]);
    }

    /**
     * @Route("/acerca-de", name="dwes_about")
     */
    public function about()
    {
        return $this->render('about.html.twig');
    }
}