<?php

namespace App\Controller;

use App\Entity\Contacto;
use App\Repository\ContactoRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactoController extends AbstractController
{
    /**
     * @Route("/contactos", name="dwes_contactos_listar")
     */
    public function index(ContactoRepository $contactoRepository): Response
    {
        $contactos = $contactoRepository->findAll();

        return $this->render('contacto/index.html.twig', [
            'contactos' => $contactos,
        ]);
    }

    /**
     * @Route("/contactos/{id}", name="dwes_contactos_show")
     */
    public function show(Contacto $contacto): Response
    {
        return $this->render('contacto/show.html.twig', [
            'contacto' => $contacto,
        ]);
    }
}
