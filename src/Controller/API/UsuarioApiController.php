<?php

namespace App\Controller\API;

use App\BLL\UsuarioBLL;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UsuarioApiController extends BaseApiController
{
    /**
     * @Route("/auth/register.{_format}",
     *     name="register",
     *     requirements={"_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"POST"}
     * )
     */
    public function register(Request $request, UsuarioBLL $userBLL)
    {
        $data = $this->getContent($request);
        $user = $userBLL->nuevo($data['email'], $data['password']);

        return $this->getResponse($user, Response:: HTTP_CREATED );
    }

    /**
     * @Route("/profile.{_format}",
     *     name="profile",
     *     requirements={"_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"GET"}
     * )
     */
    public function profile(UsuarioBLL $usuarioBLL)
    {
        $usuario = $usuarioBLL->profile();

        return $this->getResponse($usuario);
    }

    /**
     * @Route("/profile/password.{_format}",
     *     name="cambia_password",
     *     requirements={"_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"PATCH"}
     * )
     */
    public function cambiaPassword(Request $request, UsuarioBLL $usuarioBLL)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['password']) || !isset($data['password']) || empty($data['password']))
            throw new BadRequestHttpException('No se ha recibido el password');
        $user = $usuarioBLL->cambiaPassword($data['password']);
        return $this->getResponse($user);
    }
}