<?php

namespace App\Controller\CRUD;

use App\BLL\ContactoBLL;
use App\Entity\Contacto;
use App\Form\ContactoType;
use App\Repository\ContactoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/contacto-crud")
 */
class ContactoCRUDController extends AbstractController
{
    /**
     * @Route("/busqueda", name="dwes_contacto_crud_index_busqueda", methods={"POST"})
     */
    public function busqueda(
        Request $request,
        ContactoRepository $contactoRepository): Response {
        $nombre = $request->request->get('busqueda');
        $fechaInicial = $request->request->get('fechaInicial');
        $fechaFinal = $request->request->get('fechaFinal');
        $ciudad = $request->request->get('ciudad');

        $usuarioLogueado = $this->getUser();

        $contactos = $contactoRepository->findContactos(
            $usuarioLogueado, $nombre, $fechaInicial, $fechaFinal, $ciudad, $telefono=null, $order=null);

        return $this->render('contacto_crud/index.html.twig', [
            'contactos' => $contactos,
            'nombre' => $nombre,
            'fechaInicial' => $fechaInicial,
            'fechaFinal' => $fechaFinal,
            'ciudad' => $ciudad
        ]);
    }

    /**
     * @Route("/", name="dwes_contacto_crud_index", methods={"GET"})
     * @Route("/orden/{ordenacion}", name="dwes_contacto_crud_index_ordenada", methods={"GET"})
     */
    public function index(
        ContactoBLL $contactoBLL,
        string $ordenacion=null): Response
    {
        $contactos = $contactoBLL->getContactosConOrdenacion($ordenacion);

        return $this->render('contacto_crud/index.html.twig', [
            'contactos' => $contactos,
        ]);
    }

    /**
     * @Route("/new", name="dwes_contacto_crud_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $contacto = new Contacto();
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $imagen */
            $imagen = $form['imagen']->getData();

            // Generate a unique name for the file before saving it
            $fileName = md5 ( uniqid ()).'.'.$imagen->guessExtension();
// Move the file to the directory where brochures are stored
            $imagen->move(
                $this->getParameter('images_directory'),
                $fileName
            );
// Update the 'brochure' property to store the PDF file name
// instead of its contents
            $contacto->setImagen($fileName);
            $usuario = $this->getUser();
            $contacto->setUsuario($usuario);

            $entityManager->persist($contacto);
            $entityManager->flush();

            $this->addFlash('mensaje', 'Se ha creado el nuevo contacto ' . $contacto->getNombre());

            return $this->redirectToRoute('dwes_contacto_crud_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contacto_crud/new.html.twig', [
            'contacto' => $contacto,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="dwes_contacto_crud_show", methods={"GET"})
     */
    public function show(Contacto $contacto): Response
    {
        return $this->render('contacto_crud/show.html.twig', [
            'contacto' => $contacto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dwes_contacto_crud_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Contacto $contacto, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('dwes_contacto_crud_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contacto_crud/edit.html.twig', [
            'contacto' => $contacto,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="dwes_contacto_crud_delete", methods={"POST"})
     */
    public function delete(Request $request, Contacto $contacto, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($this->isCsrfTokenValid('delete'.$contacto->getId(), $request->request->get('_token'))) {
            $entityManager->remove($contacto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dwes_contacto_crud_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}", name="dwes_contacto_crud_delete_json", methods={"DELETE"})
     */
    public function deleteJson(Contacto $contacto, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($contacto);
        $entityManager->flush();

        return new JsonResponse(['eliminado' => true]);
    }
}
